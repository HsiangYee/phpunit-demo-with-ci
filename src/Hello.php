<?php

namespace App;

class Hello
{
    public function say($name)
    {
        return 'Hello ' . $name;
    }
}
